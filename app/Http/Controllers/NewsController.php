<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;

use App\News;

class NewsController extends Controller
{
    //
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index() {
     	$news = News::all()->first();


        return view('admin.news.index')->with([
            'url' => 'admin-news',

            'news' => $news,
        ]);
    }

    public function updateNews(Request $request) {
    	$news = News::all()->first();

    	if ($news) {
    		$news->content = $request->input('content');
    		$news->save();	
    	} else {
    		$news = new News;
    		$news->content = $request->input('content');
    		$news->save();
    	}
    	
    	return redirect('/admin-news')->with('success_message', 'Cập nhật news thành công');
    }
}
