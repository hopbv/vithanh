@extends('layouts.app')

@section('title')
<title>Miếng Thấm Hút Thực Phẩm Cao Cấp - Liên hệ</title>
@endsection

@section('content')
<section id="body-section-contact">
    <div class="img-container" style="background-image: url({{  $contact->url }})">
        <!-- <img src="img/slide-2.png" alt=""> -->
    </div>
    <section id="contact">
        <div class="row">
            <div class="col-sm-1"></div>
            <div class="content-container col-sm-10">
                <div class="header">
                    <div class="start-line">
                        <div class="line-top"></div>
                        <div class="content">LIÊN HỆ</div>
                    </div>
                </div>
                <div class="intro">{!!($contact) ? $contact->content : ''!!}
                </div>
            </div>
        </div>
    </section>
</section>
@endsection