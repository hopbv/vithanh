<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use File;

use App\Contact;

use App\Product;
use App\Tag;

class ContactController extends Controller
{
    //
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index() {
     	$contact = Contact::all()->first();

        $products = Product::all();
        $tags = Tag::all();


        return view('admin.contact.index')->with([
            'url' => 'admin-contact',

            'contact' => $contact,
            'products' => $products,
            'tags' => $tags
        ]);
    }

    public function updateContact(Request $request) {
    	$contact = Contact::all()->first();

    	if ($contact) {
    		$contact->content = $request->input('content');

            $files = $request->file('images');
            if($request->hasFile('images')) {
                foreach ($files as $file) {
                  $fileName = substr(md5($file->getClientOriginalName().date("Y-m-d h:i:sa")), 15).'.'.$file->getClientOriginalExtension();
                  $file->move('uploads', $fileName);

                  File::delete(public_path().$contact->url);
                  $contact->url = '/uploads/'. $fileName;

                }
            }

    		$contact->save();	
    	} else {
    		$contact = new Contact;
    		$contact->content = $request->input('content');
    		$contact->save();
    	}
    	
    	return redirect('/admin-contact')->with('success_message', 'Cập nhật contact thành công');
    }
}
