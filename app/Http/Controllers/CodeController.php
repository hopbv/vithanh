<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use File;

use App\Product;
use App\Tag;

use App\Code;

class CodeController extends Controller
{
    //
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function editCode() {
    	$products = Product::all();
    	$tags = Tag::all();

    	$code = Code::all()->first();

    	return view('admin.code.edit')->with([
    		'url' => 'admin-code-edit',

    		'code' => $code,

    		'products' => $products,
    		'tags' => $tags
    	]);
    }

    public function updateCode(Request $request) {
    	$code = Code::all()->first();

    	if ($code) {
    		$code->code = $request->input('code');
    		$code->save();
    	} else {
    		$code = new Code();
    		$code->code = $request->input('code');
    		$code->save();
    	}

    	return redirect('/admin-code')->with('success_message', 'Cập nhật code thành công!');
    }
}
