@extends('layouts.admin')

@section('header')

    <!-- <link href="{{asset('css/plugins/summernote/summernote-bs3.css')}}" rel="stylesheet"> -->
    <!-- Sweet Alert -->
    <link href="{{ asset('frontend/css/plugins/sweetalert/sweetalert.css') }}" rel="stylesheet">

    <link href="{{ asset('frontend/css/plugins/slick/slick.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/plugins/slick/slick-theme.css') }}" rel="stylesheet">
    <style type="text/css">
        .container {
            position: relative;
            width: 50%;
        }

        .image {
          opacity: 1;
          display: block;
          width: 100%;
          height: auto;
          transition: .5s ease;
          backface-visibility: hidden;
        }

        .middle {
          transition: .5s ease;
          opacity: 0;
          position: absolute;
          top: 50%;
          left: 50%;
          transform: translate(-50%, -50%);
          -ms-transform: translate(-50%, -50%);
          text-align: center;
        }

        .container:hover .image {
          opacity: 0.3;
        }

        .container:hover .middle {
          opacity: 1;
        }
    </style>
@endsection

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Cập nhật sliders</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/admin">Admin</a>
            </li>
            <li>
                <a href="/admin-slider">Quản lý sliders trang Home</a>
            </li>
            <li class="active">
                <strong>Cập nhật sliders</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Thêm sliders mới</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form class="form-horizontal" method="POST" action="/admin-slider/add" enctype="multipart/form-data">
                        <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                        
                        <div class="form-group"><label class="col-lg-2 control-label">Hình ảnh/ vdeo (mp4)</label>
                            <div class="col-lg-10">
                                <input accept="image/*|video/*" multiple="multiple" type="file" class="slider-img-input" name="images[]">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-12">
                                <button type="submit" class="btn btn-primary pull-right">Đăng hình ảnh/ video</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Danh sách sliders</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                
                        <div class="slick_demo_2">
                            @if(count($sliders) > 0)
                                    @foreach ($sliders as $slider)
                                        <div class="container">
                                            <!-- <div class="ibox-content"> -->
                                                @php
                                                    $extension = explode('.',$slider->url)[1];
                                                @endphp
                                                @if ($extension == 'png' || $extension == 'jpg' || $extension == 'jpeg')
                                                    <img src="{{$slider->url}}" width="80%" class="image">
                                                    
                                                @endif
                                                @if ($extension == 'mp4')
                                                    <video>
                                                        <source src="{{$slider->url}}" width="80%" type="video/mp4">
                                                    </video>
                                                @endif
                                                <div class="middle">
                                                    <button class="btn btn-danger btn-delete" data-id="{{$slider->id}}">Xóa</button>
                                                </div>
                                            <!-- </div> -->
                                        </div>
                                    @endforeach
                            @endif
                            
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script src="{{ asset('frontend/js/jquery-3.1.1.min.js')}}"></script>
    <script src="{{ asset('frontend/js/bootstrap.min.js')}}"></script>
    <script src="{{ asset('frontend/js/plugins/metisMenu/jquery.metisMenu.js')}}"></script>
    <script src="{{ asset('frontend/js/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>
    
    <!-- Sweet alert -->
    <script src="{{ asset('frontend/js/plugins/sweetalert/sweetalert.min.js') }}"></script>

    <!-- slick carousel-->
    <script src="{{ asset('frontend/js/plugins/slick/slick.min.js') }}"></script>

    <!-- Additional style only for demo purpose -->
    <style>
        .slick_demo_2 .ibox-content {
            margin: 0 10px;
        }
    </style>
    <script>
        $(document).ready(function(){

            $('.slick_demo_2').slick({
                infinite: true,
                slidesToShow: 3,
                slidesToScroll: 1,
                centerMode: true,
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 3,
                            infinite: true,
                            dots: true
                        }
                    },
                    {
                        breakpoint: 600,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                ]
            });

        });

    </script>

    <script>
        $('.btn-delete').click(function () {
            var id = $(this).data('id');
            swal({
                        title: "Bạn có chắc chắn xóa không?",
                        text: "Hình ảnh sau khi xóa sẽ không hồi phục lại được.",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Có",
                        cancelButtonText: "Không",
                        closeOnConfirm: false,
                        closeOnCancel: false },
                    function (isConfirm) {
                        if (isConfirm) {
                            swal("Đã xóa!", "Hình ảnh slider đã được xóa.", "success");
                            window.location.href="/admin-slider/delete/"+id;
                        } else {
                            swal("Đã hủy", "Hình ảnh slider đã được giữ lại", "error");
                        }
                    });
        });
    </script>

    
@endsection
