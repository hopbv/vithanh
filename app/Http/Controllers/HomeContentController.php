<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;

use App\HomeContent;
use App\Product;
use App\Tag;

class HomeContentController extends Controller
{
    //
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index() {
     	$homeContent = HomeContent::all()->first();
        $products = Product::all();
        $tags = Tag::all();


        return view('admin.home.index')->with([
            'url' => 'admin-home',

            'homeContent' => $homeContent,
            'products' => $products,
            'tags' => $tags
        ]);
    }

    public function updateHomeContent(Request $request) {
    	$homeContent = HomeContent::all()->first();

    	if ($homeContent) {
    		$homeContent->content = $request->input('content');
    		$homeContent->save();	
    	} else {
    		$homeContent = new HomeContent;
    		$homeContent->content = $request->input('content');
    		$homeContent->save();
    	}
    	
    	return redirect('/admin-home')->with('success_message', 'Cập nhật home thành công');
    }
}
