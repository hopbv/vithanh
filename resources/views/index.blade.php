@extends('layouts.app')

@section('title')
<title>Miếng Thấm Hút Thực Phẩm Cao Cấp</title>
@endsection

@section('content')
<section id="body-section-product">
    <section id="slide-container">
        <div class="left-arrow">
            <div class="arrow-container">
                <img src="{{ asset('homepage/img/2.png') }}">
            </div>
        </div>
        <div id="brand-carousel">
            @if (count($sliders) > 0)
                @foreach($sliders as $slider)
                    <div class="brand-item-carousel">   
                        @php
                            $extension = explode('.',$slider->url)[1];
                        @endphp
                        @if ($extension == 'png' || $extension == 'jpg' || $extension == 'jpeg')
                            <div class="img-container" style="background-image: url('{{$slider->url}}')">
                                <!-- <img src="img/slide-1.png"> -->
                            </div>
                        @endif
                        @if ($extension == 'mp4')
                            <div class="fullscreen-bg">
                                
                                <video class="fullscreen-bg-video" loop muted controls>
                                    <source src="{{$slider->url}}" type="video/mp4">
                                </video>    
                            </div>
                            
                        @endif
                    </div>
                @endforeach
            @endif
            
        </div>
        <div class="right-arrow">
            <div class="arrow-container">
                <img src="{{ asset('homepage/img/1.png') }}">
            </div>
        </div>
    </section>
    <section id="solution-section">
        <div class="row">
            <div class="col-sm-1"></div>
            <div class="content col-sm-10">
                <div class="header">
                    <div class="description">
                        <p class="title1">Giải pháp</p>
                        <p class="title2">THẤM HÚT VÀ KHỬ MÙI THỰC PHẨM</p>
                        <div class="end-line">
                            <p class="title3">
                                từ tập đoàn SIRANE
                            </p>
                            <div class="line-bottom"></div>
                        </div>
                    </div>
                </div>
                <div class="intro">{!!($homeContent) ? ($homeContent->content) : ''!!}
                </div>
                <div class="solution-list row">
                    @php
                        $count = 0;
                    @endphp
                    @if (count($tags) > 0)
                        @foreach($tags as $tag)
                            @php
                                $count++;
                            @endphp
                            <div class="solution-item col-sm-5 solution{{$tag->id}}" style="background-image: url('{{$tag->url}}');">
                                <a href="{{($tag->blog_url) ? $tag->blog_url : '' }}">
                                    <div class="outlayer">
                                        <div class="item-header">
                                            <div class="start-line">
                                                <div class="line-top"></div>
                                                <p class="header1">Giải pháp</p>
                                            </div>
                                            <p class="header2">{{$tag->value}}</p>
                                        </div>
                                        <div class="icon">
                                            <img src="{{$tag->url_icon}}" alt="">
                                        </div>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </section>
    <section id="article-section-news">
        <div class="header">
            <div class="row">
                <div class="col-sm-1"></div>
                <div class="content-container title col-sm-10">
                    <div class="start-line">
                        <div class="line-top"></div>
                        <div class="content">BÀI VIẾT CHỌN LỌC</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="article-list">
            @if (count($blogs) > 0)
                @foreach($blogs as $blog)
                    <div class="row">
                        <div class="col-sm-1"></div>
                        <a href="#">
                            <div class="header col-sm-10">
                                <div class="row">
                                    <div class="img-container col-sm-6">
                                        <div class="end-line">
                                            <a href="{{$blog->url}}"><img src="{{$blog->images[0]->url}}"></a>
                                            <div class="line-bottom"></div>
                                        </div>
                                    </div>
                                    <div class="content-container col-sm-6">
                                        <div class="paragraph"><a href="{{$blog->url}}" style="text-decoration: none;">{{$blog->name}}</a></div>
                                        <div class="description-article">{!!mb_substr(strip_tags($blog->description), 0, 200)!!}...</div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                @endforeach
            @endif
        </div>
    </section>
</section>
@endsection