<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Vi Thanh| Admin Page</title>

    <link href="{{ asset('frontend/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/font-awesome/css/font-awesome.css') }}" rel="stylesheet">

    <!-- Toastr style -->
    <link href="{{ asset('frontend/css/plugins/toastr/toastr.min.css') }}" rel="stylesheet">

    <link href="{{ asset('frontend/css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/custom.css') }}" rel="stylesheet">
    <link rel="shortcut icon" href="https://miengthamthucpham.vn/homepage/img/logo.png" type="image/x-icon"/>
    <meta property="og:image" content="https://yt3.ggpht.com/a-/ACSszfG6SiS4096AdxOv4vjhBXJphsGQuBWBBwkLww=s900-mo-c-c0xffffffff-rj-k-no"/>
	<meta property="og:image:secure_url" content="https://yt3.ggpht.com/a-/ACSszfG6SiS4096AdxOv4vjhBXJphsGQuBWBBwkLww=s900-mo-c-c0xffffffff-rj-k-no" />
    @yield('header')
</head>

<body>

<div id="wrapper">

    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element">
                        <span>
                            <img alt="image" class="img-circle" src="{{ asset('frontend/img/user-profile.png') }}" style="width: 50px;"/>
                        </span>
                        <span class="block m-t-xs"> <strong class="font-bold">{{Auth::user()->name}}</strong></span>
                    </div>
                </li>
                <li class="{{($url == 'admin-product' ||  $url == 'admin-product-edit-1' || $url == 'admin-product-edit-2' || $url == 'admin-product-edit-3' || $url == 'admin-product-edit-4') ? 'active' : ''}}">
                    <a href="#"><i class="fa fa-cube"></i> <span class="nav-label">Quản lý sản phẩm</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        @foreach ($products as $product)
                            <li class="{{($url == 'admin-product' || $url == 'admin-product-edit-<?php echo $product->id?>') ? 'active' : ''}}"><a href="/admin-product/edit/{{$product->id}}">{{$product->name}}</a></li>
                        @endforeach
                        
                    </ul>
                </li>

                <li class="{{($url == 'admin-tag' || $url == 'admin-tag-edit-1' || $url == 'admin-tag-edit-2' || $url == 'admin-tag-edit-3' || $url == 'admin-tag-edit-4') ? 'active' : ''}}">
                    <a href="#"><i class="fa fa-tags"></i> <span class="nav-label">Quản lý category bài viết</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        @foreach ($tags as $tag)
                            <li class="{{($url == 'admin-tag' || $url == 'admin-tag-edit-<?php echo $tag->id?>') ? 'active' : ''}}"><a href="/admin-tag/edit/{{$tag->id}}">{{$tag->value}}</a></li>
                        @endforeach
                    </ul>
                </li>

                <li class="{{($url == 'admin-blog' || $url == 'admin-blog' || $url == 'admin-blog-edit') ? 'active' : ''}}">
                    <a href="/admin-blog"><i class="fa fa-file"></i> <span class="nav-label">Danh sách bài viết</span></a>
                </li>

                <li class="{{($url == 'admin-home' || $url == 'admin-home-edit') ? 'active' : ''}}">
                    <a href="/admin-home"><i class="fa fa-home"></i> <span class="nav-label">Quản lý trang Home</span></a>
                </li>

                <li class="{{($url == 'admin-contact' || $url == 'admin-contact-edit') ? 'active' : ''}}">
                    <a href="/admin-contact"><i class="fa fa-comments"></i> <span class="nav-label">Quản lý trang Contact</span></a>
                </li>

                <li class="{{($url == 'admin-slider' || $url == 'admin-slider-edit') ? 'active' : ''}}">
                    <a href="/admin-slider"><i class="fa fa-image"></i> <span class="nav-label">Quản lý slider trang Home</span></a>
                </li>

                <li class="{{($url == 'admin-product-slider' || $url == 'admin-product-slider-edit') ? 'active' : ''}}">
                    <a href="/admin-product-slider"><i class="fa fa-image"></i> <span class="nav-label">Quản lý slider trang Product</span></a>
                </li>

                <li class="{{($url == 'admin-code' || $url == 'admin-code-edit') ? 'active' : ''}}">
                    <a href="/admin-code"><i class="fa fa-code"></i> <span class="nav-label">Quản lý code</span></a>
                </li>
                

            </ul>
        </div>
    </nav>

    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                <ul class="nav navbar-top-links navbar-right">
                    
                    <li>
                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            <i class="fa fa-sign-out"></i> Log out
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                </ul>

            </nav>
        </div>
        @yield('content')
        <div class="footer">
            <div>
                <strong>Copyright</strong> <a href="https://pixiostudio.com/">Pixio Studio Company</a> &copy; 2018
            </div>
        </div>
    </div>
</div>


<!-- Mainly scripts -->
<script src="{{ asset('frontend/js/jquery-3.1.1.min.js') }}"></script>
<script src="{{ asset('frontend/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('frontend/js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
<script src="{{ asset('frontend/js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>

<!-- Custom and plugin javascript -->
<script src="{{ asset('frontend/js/inspinia.js') }}"></script>
<script src="{{ asset('frontend/js/plugins/pace/pace.min.js') }}"></script>
<!-- Toastr -->
<script src="{{ asset('frontend/js/plugins/toastr/toastr.min.js') }}"></script>

<script>
    $(document).ready(function() {
        @if(session('success_message'))
            setTimeout(function() {
                toastr.options = {
                    closeButton: true,
                    progressBar: true,
                    showMethod: 'slideDown',
                    timeOut: 1200
                };
                toastr.success('{{session('success_message')}}', 'Vi Thanh Admin');
            }, 300);
        @endif
        @if(session('danger_message'))
            setTimeout(function() {
                toastr.options = {
                    closeButton: true,
                    progressBar: true,
                    showMethod: 'slideDown',
                    timeOut: 1200
                };
                toastr.danger('{{session('danger_message')}}', 'Vi Thanh Admin');
            }, 300);
        @endif
    });
</script>
@yield('script')
</body>

</html>
