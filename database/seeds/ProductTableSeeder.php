<?php

use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('products')->insert([
            'name' => 'Miếng thấm hút',
            'tag' => 'Thực phẩm'
        ]);

        DB::table('products')->insert([
            'name' => 'Miếng khử mùi',
            'tag' => 'Thực phẩm'
        ]);

        DB::table('products')->insert([
            'name' => 'Miếng bảo quản',
            'tag' => 'Thực phẩm'
        ]);

        DB::table('products')->insert([
            'name' => 'Túi nấu ăn',
            'tag' => 'Tiện dụng'
        ]);
    }
}
