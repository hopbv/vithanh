<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use File;

use App\Slider;

use App\Product;
use App\Tag;

class SliderController extends Controller
{
    //
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index() {
     	$sliders = Slider::all();

      $products = Product::all();
      $tags = Tag::all();


        return view('admin.slider.index')->with([
            'url' => 'admin-slider',

            'products' => $products,
            'tags' => $tags,
            'sliders' => $sliders,
        ]);
    }

    public function addSliders(Request $request) {
    	// save blog images
        $files = $request->file('images');
        if($request->hasFile('images')) {
            foreach ($files as $file) {
              $fileName = substr(md5($file->getClientOriginalName().date("Y-m-d h:i:sa")), 15).'.'.$file->getClientOriginalExtension();
              $file->move('uploads', $fileName);
              $slider = new Slider;
              $slider->url = '/uploads/'. $fileName;


              $slider->save();
            }
        }

        return redirect('/admin-slider')->with('success_message', 'Thêm sliders thành công');
    }

    public function deleteSlider($id) {
    	$slider = Slider::find($id);

    	File::delete(public_path().$slider->url);
    	$slider->delete();

    	return redirect('/admin-slider')->with('success_message', 'Xóa sliders thành công');
    }
}
