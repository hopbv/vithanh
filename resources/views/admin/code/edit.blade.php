@extends('layouts.admin')

@section('header')
    <link href="{{ asset('frontend/css/plugins/jasny/jasny-bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/plugins/codemirror/codemirror.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/plugins/codemirror/ambiance.css') }}" rel="stylesheet">

    <!-- Sweet Alert -->
    <link href="{{ asset('frontend/css/plugins/sweetalert/sweetalert.css') }}" rel="stylesheet">


@endsection

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Cập nhật code</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/admin">Admin</a>
            </li>
            <li>
                <a href="/admin-code">Quản lý code</a>
            </li>
            <li class="active">
                <strong>Cập nhật code</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Cập nhật code</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form class="form-horizontal" method="POST" action="/admin-code/update" enctype="multipart/form-data">
                        <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                        <textarea id="code" name="code">
{{($code) ? $code->code : '// Code goes here'}}    
                        </textarea>
                        <br>
                        <div class="form-group">
                            <div class="col-lg-12">
                                <button type="submit" class="btn btn-primary pull-right">Cập nhật</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script src="{{ asset('frontend/js/jquery-3.1.1.min.js')}}"></script>
    <script src="{{ asset('frontend/js/bootstrap.min.js')}}"></script>
    <script src="{{ asset('frontend/js/plugins/metisMenu/jquery.metisMenu.js')}}"></script>
    <script src="{{ asset('frontend/js/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>
    <!-- Jasny -->
    <script src="{{ asset('frontend/js/plugins/jasny/jasny-bootstrap.min.js') }}"></script>
    <!-- CodeMirror -->
    <script src="{{ asset('frontend/js/plugins/codemirror/codemirror.js') }}"></script>
    <script src="{{ asset('frontend/js/plugins/codemirror/mode/xml/xml.js') }}"></script>
    <script src="{{ asset('frontend/js/plugins/codemirror/mode/javascript/javascript.js') }}"></script>

    <script>
        $(document).ready(function() {
            var editor = CodeMirror.fromTextArea(document.getElementById("code"), {
                 lineNumbers: true,
                 matchBrackets: true,
                 styleActiveLine: true
             });
        })
    </script>
@endsection
