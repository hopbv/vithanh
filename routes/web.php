<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('/user-register', 'UserController@register');
Route::post('/user-login', 'UserController@login');

Route::get('/', 'FrontendController@home');
Route::get('/news', 'FrontendController@news');
Route::get('/product', 'FrontendController@product');
Route::get('/contact', 'FrontendController@contact');
Route::get('/blog/{name}', 'FrontendController@blog');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/admin', 'ProductController@index');

Route::get('/admin-product', 'ProductController@index');
// Route::post('/admin-product/add', 'ProductController@addProduct');
Route::get('/admin-product/edit/{id}', 'ProductController@editProduct');
Route::post('/admin-product/update/{id}', 'ProductController@updateProduct');
// Route::get('/admin-product/delete/{id}', 'ProductController@deleteProduct');

Route::get('/admin-tag', 'TagController@index');
Route::get('/admin-tag/edit/{id}', 'TagController@editTag');
Route::post('/admin-tag/update/{id}', 'TagController@updateTag');


Route::get('/admin-blog', 'BlogController@index');
Route::post('/admin-blog/add', 'BlogController@addBlog');
Route::get('/admin-blog/edit/{id}', 'BlogController@editBlog');
Route::post('/admin-blog/update/{id}', 'BlogController@updateBlog');
Route::get('/admin-blog/delete/{id}', 'BlogController@deleteBlog');

Route::get('/admin-home', 'HomeContentController@index');
Route::post('/admin-home/update', 'HomeContentController@updateHomeContent');

Route::get('/admin-contact', 'ContactController@index');
Route::post('/admin-contact/update', 'ContactController@updateContact');

Route::get('/admin-slider', 'SliderController@index');
Route::post('/admin-slider/add', 'SliderController@addSliders');
Route::get('/admin-slider/delete/{id}', 'SliderController@deleteSlider');

Route::get('/admin-product-slider', 'ProductSliderController@index');
Route::post('/admin-product-slider/add', 'ProductSliderController@addSliders');
Route::get('/admin-product-slider/delete/{id}', 'ProductSliderController@deleteSlider');


Route::get('/admin-blog-tag/addFeatured/{isChecked}/{tagId}/{blogId}', 'TagController@addFeatured');
Route::post('/admin-image/create-img-url', 'ImageController@createImgUrl');
Route::post('/admin-image/delete-img', 'ImageController@deleteImg');


Route::get('/admin-code', 'CodeController@editCode');
Route::post('/admin-code/update', 'CodeController@updateCode');