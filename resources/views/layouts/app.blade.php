<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    @yield('title')
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="{{ asset('frontend/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('homepage/css/main.css') }}" />
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('homepage/css/slick.css') }}" />
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('homepage/css/slick-theme.css') }}" />
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.css">
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('homepage/css/fonts.css') }}" />
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('homepage/css/responsive.css') }}" />
    <link rel="shortcut icon" href="https://miengthamthucpham.vn/homepage/img/logo.png" type="image/x-icon"/>
    <meta name="description" content="Hút sạch chất lỏng gây hại, trả lại vẻ tươi ngon cho thịt bò, thịt gà, thịt heo, cá biển...">
    <meta property="og:image" content="https://miengthamthucpham.vn/homepage/img/slide-1.png"/>
    <meta property="og:image:secure_url" content="https://miengthamthucpham.vn/homepage/img/slide-1.png">

    {!!$code->code!!}
    
</head>

<body>
    <section id="section-header">
        <div class="row">
            <div class="col-sm-1"></div>
            <div class="col-sm-10">
                <div class="logo col-sm-4">
                    <div class="logo-container">
                        <a href="/"><img src="{{ asset('homepage/img/logo.png') }}" alt=""></a>
                    </div>
                </div>
                <div class="navbar-container col-sm-8">
                    <ul class="navbar-content nav navbar-nav navbar-right">
                        <li><a href="/">HOME</a></li>
                        <li><a href="/product">PRODUCT</a></li>
                        <li><a href="/contact">CONTACT</a></li>
                    </ul>
                </div>
                <div class="navbar-container-mobile col-sm-6">
                    <nav class="navbar navbar-inverse col-sm-7">
                    <!-- <div class="container-fluid"> -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <!-- </div> -->
                    </nav>
                </div>
            </div>
            <div class="col-sm-1"></div>
            <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav">
                    <li><a href="/">HOME</a></li>
                    <li><a href="/product">PRODUCT</a></li>
                    <li><a href="/contact">CONTACT</a></li>
                </ul>
            </div>
        </div>
    </section>

    @yield('content')
    

    <section id="footer-section">
        <div class="row">
            <div class="content">
                <span>Hotline/Zalo: 0935 007 997 (Mr. Cường)</span>
                </div>
            <div class="col-sm-1 col-xs-1"></div>
            <!-- <div id="partner-carousel" class="col-sm-10 col-xs-10">
                <div class="partner-item">
                    <img src="{{ asset('homepage/img/brand-1.png') }}" alt="">
                </div>
                <div class="partner-item">
                    <img src="{{ asset('homepage/img/brand-2.png') }}" alt="">
                </div>
                <div class="partner-item">
                    <img src="{{ asset('homepage/img/brand-3.png') }}" alt="">
                </div>
                <div class="partner-item">
                    <img src="{{ asset('homepage/img/brand-4.png') }}" alt="">
                </div>
                <div class="partner-item" style="margin-right: 0">
                    <img src="{{ asset('homepage/img/brand-5.png') }}" alt="">
                </div>
                <div class="partner-item">
                    <img src="{{ asset('homepage/img/brand-1.png') }}" alt="">
                </div>
                <div class="partner-item">
                    <img src="{{ asset('homepage/img/brand-2.png') }}" alt="">
                </div>
                <div class="partner-item">
                    <img src="{{ asset('homepage/img/brand-3.png') }}" alt="">
                </div>
                <div class="partner-item">
                    <img src="{{ asset('homepage/img/brand-4.png') }}" alt="">
                </div>
                <div class="partner-item" style="margin-right: 0">
                    <img src="{{ asset('homepage/img/brand-5.png') }}" alt="">
                </div>
            </div> -->
        </div>
    </section>
    
    <!-- jQuery library -->
    <script src="{{ asset('frontend/js/jquery.min.js') }}"></script>
    <!-- Latest compiled JavaScript -->
    <script src="{{ asset('frontend/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('homepage/js/slick.min.js') }}"></script>
    <script src="{{ asset('homepage/js/main.js') }}"></script>
</body>

</html>