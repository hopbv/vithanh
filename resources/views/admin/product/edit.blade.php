@extends('layouts.admin')

@section('header')

    <link href="{{ asset('frontend/css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/plugins/summernote/summernote.css')}}" rel="stylesheet">


@endsection

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Cập nhật sản phẩm</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/admin">Admin</a>
            </li>
            <li>
                <a href="/admin-product">Quản lý sản phẩm</a>
            </li>
            <li class="active">
                <strong>Cập nhật sản phẩm</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Cập nhật sản phẩm</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form class="form-horizontal" method="POST" action="/admin-product/update/{{$product->id}}" enctype="multipart/form-data">
                        <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                        <div class="form-group"><label class="col-lg-2 control-label">Tên sản phẩm</label>
                            <div class="col-lg-10"><input type="text" placeholder="product" name="name" class="form-control" value="{{$product->name}}">
                            </div>
                        </div>
                        <div class="form-group"><label class="col-lg-2 control-label">Tag</label>
                            <div class="col-lg-10"><input type="text" name="tag" class="tagsinput form-control" value="{{$product->tag}}">
                            </div>
                        </div>
                        <div class="form-group"><label class="col-lg-2 control-label">Chi tiết</label>
                            <div class="col-lg-10">
                                <textarea type="text" placeholder="description" name="description" style="height: 100px;" class="form-control summernote">{{($product->description) ? $product->description : ''}}</textarea>
                            </div>
                        </div>
                        <div class="form-group"><label class="col-lg-2 control-label">Thumbnail</label>
                            <div class="col-lg-10">
                                <input accept="image/*" type="file" class="product-img-input" name="images[]">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-2"></div>
                            <div class="col-lg-10">
                                <img src="{{(count($productImage)>0) ? $productImage[0]->url : ''}}" height="100px">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-12">
                                <button type="submit" class="btn btn-primary pull-right">Cập nhật</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script src="{{ asset('frontend/js/jquery-3.1.1.min.js')}}"></script>
    <script src="{{ asset('frontend/js/bootstrap.min.js')}}"></script>
    <script src="{{ asset('frontend/js/plugins/metisMenu/jquery.metisMenu.js')}}"></script>
    <script src="{{ asset('frontend/js/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>

    <!-- Tags Input -->
    <script src="{{ asset('frontend/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js') }}"></script>
    

    <!-- SUMMERNOTE -->
    <script src="{{ asset('frontend/js/plugins/summernote/summernote.min.js')}}"></script>
    <script>
        $(document).ready(function() {
            
            $('.tagsinput').tagsinput({
                tagClass: 'label label-primary'
            });
            // $('.chosen-select').chosen({width: "100%"});
            $('.summernote').summernote({
                
                height:500,
                fontSizes: ['8', '9', '10', '11', '12', '14', '18','20','24','28','30','32','36','40'],
                toolbar: [
                    // [groupName, [list of button]]
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['font', ['strikethrough', 'superscript', 'subscript']],
                    ['fontsize', ['fontsize']],
                    ['fontname', ['fontname']],
                    ['style', ['style']],   
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['height', ['height']],
                    ['insert', ['link','image', 'doc', 'video', 'picture']], // image and doc are customized buttons
                  ],
                callbacks: {
                    onImageUpload: function(image) {
                        uploadImage(image[0]);
                    },
                    onMediaDelete : function ($target, $editable) {
                        // console.log();   // get image url 
                        deleteImage($target.attr('src'));

                    },
                    onPaste: function (e) {
                         var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
                         e.preventDefault();
                         setTimeout(function () {
                           document.execCommand('insertText', false, bufferText);
                         }, 10);
                       }
                }
            });

            function deleteImage(url) {
              var data = new FormData();
              data.append("url", url);
              var token = $('input[name=_token]').val();
              $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': token
                }
              });
              $.ajax({
                  url: '/admin-image/delete-img',
                  cache: false,
                  contentType: false,
                  processData: false,
                  data: data,
                  type: "post",

                  success: function(data) {
                    console.log(data);
                  },
                  error: function(data) {
                    console.log(data);
                  }
              });  
            }

            function uploadImage(image) {
              var data = new FormData();
              data.append("image", image);
              var token = $('input[name=_token]').val();
              $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': token
                }
              });
              $.ajax({
                  url: '/admin-image/create-img-url',
                  cache: false,
                  contentType: false,
                  processData: false,
                  data: data,
                  type: "post",
                  success: function(data) {
                      
                      var url = data['url'];
                      var image = $('<img>').attr({src: url, width: '100%'});
                      $('.summernote').summernote("insertNode", image[0]);
                  },
                  error: function(data) {
                      console.log(data);
                  }
              });
            }
            $('.dropdown-toggle').dropdown();
        });
    </script>
@endsection
