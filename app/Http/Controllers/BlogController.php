<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use File;


use App\Blog;
use App\BlogImage;

use App\Product;

use App\Tag;
use App\BlogTag;

class BlogController extends Controller
{
    //
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index() {
      $products = Product::all();
      $blogs = Blog::all();
      $tags = Tag::all();

      foreach ($blogs as $blog) {
        $blog['images'] = BlogImage::where('blog_id', $blog->id)->get();

        $tag = BlogTag::where('blog_id', $blog->id)->get()[0];
        $blog['tag'] = Tag::find($tag->tag_id);
        $blog['checked'] = $tag->checked;

      }

        return view('admin.blog.index')->with([
            'url' => 'admin-blog',
            'products' => $products,

            'blogs' => $blogs,
            'tags' => $tags
        ]);
    }

    public function addBlog(Request $request) {
      // print_r($request->all());
        $blog = new Blog;
        $blog->name = $request->input('name');
        $blog->description = ($request->input('description')) ? $request->input('description') : '';
        $blog->url = $request->input('url');
        $blog->save();

        $blogTag = new BlogTag;
        $blogTag->blog_id = $blog->id;
        $blogTag->tag_id = $request->input('tag');
        $blogTag->save();
        
        // save blog images
        $files = $request->file('images');
        if($request->hasFile('images')) {
            foreach ($files as $file) {
              $fileName = substr(md5($file->getClientOriginalName().date("Y-m-d h:i:sa")), 15).'.'.$file->getClientOriginalExtension();
              $file->move('uploads', $fileName);
              $blogImage = new BlogImage;
              $blogImage->url = '/uploads/'. $fileName;
              $blogImage->blog_id = $blog->id;

              $blogImage->save();
            }
        }

        return redirect('/admin-blog/edit/'.$blog->id)->with('success_message', 'Đăng bài viết thành công');
    }

    public function editBlog($id) {
    $blog = Blog::find($id);
    $tag1 = BlogTag::where('blog_id', $blog->id)->get()[0];
    $blog['tag'] = Tag::find($tag1->tag_id);
    $tagIds[]=$blog['tag']->id;
    $arrTags = Tag::whereNotIn('id', $tagIds)->get();

    $blogImage = BlogImage::where('blog_id', $id)->get();

    $tags = Tag::all();
    $products = Product::all();
    $blogs = Blog::all();

    $tagIds = [];
    foreach ($blogs as $blog1) {
      $blog1['images'] = BlogImage::where('blog_id', $blog1->id)->get();
      $tag = BlogTag::where('blog_id', $blog1->id)->get()[0];
      $blog1['tag'] = Tag::find($tag->tag_id);
      $blog1['checked'] = $tag->checked;
      
    }

    
    // print_r(json_encode($tagIds));


    return view('admin.blog.edit')->with([
      'url' => 'admin-blog-edit',

      'blog' => $blog,
      'blogImage' => $blogImage,

      'tags' => $tags,
      'arrTags' => $arrTags,
      'products' => $products,
      'blogs' => $blogs
    ]);
  }  


  public function updateBlog(Request $request, $id) {
    $blog = Blog::find($id);

    $blog->name = $request->input('name');
    $blog->description = $request->input('description');
    $blog->url = $request->input('url');
        
    $blog->save();

    $blogTag = BlogTag::where('blog_id', $id)->first();
    $blogTag->blog_id = $blog->id;
    $blogTag->tag_id = $request->input('tag');
    $blogTag->save();


    // save blog images
    $files = $request->file('images');
    if($request->hasFile('images')) {
        foreach ($files as $file) {
          $fileName = substr(md5($file->getClientOriginalName().date("Y-m-d h:i:sa")), 15).'.'.$file->getClientOriginalExtension();
          $file->move('uploads', $fileName);
          $blogImage = BlogImage::where('blog_id', $blog->id)->first();

          if ($blogImage)
            File::delete(public_path().$blogImage->url);
          $blogImage->url = '/uploads/'. $fileName;
          $blogImage->blog_id = $blog->id;

          $blogImage->save();
        }
    }

    return redirect('/admin-blog/edit/'.$id)->with('success_message', 'Cập nhật bài viết thành công');
  }  

    public function deleteBlog($id) {
        
        $blog = Blog::find($id);

        // unlink(public_path().'/uploads/'.$id);
        $blogImage = BlogImage::where('blog_id', $id)->first();

//        print(json_encode($blogImage));
         if ($blogImage) {
         File::delete(public_path().$blogImage->url);
          // File::delete(public_path().$blogImage->url_icon);
        }
        $blogImage->delete();

        $blogTag = BlogTag::where('blog_id', $id);
        $blogTag->delete();
        $blog->delete();
    
        return redirect('/admin-blog')->with('success_message', 'Xóa bài viết thành công');
    }
}
