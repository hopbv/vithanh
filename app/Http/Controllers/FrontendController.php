<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Slider;
use App\ProductSlider;
use App\HomeContent;
use App\Product;
use App\ProductImage;
use App\Blog;
use App\BlogImage;
use App\Contact;
use App\Tag;
use App\BlogTag;

use App\Code;

use App\Helpers\Html2Text;

class FrontendController extends Controller
{
    //
    public function home() {
        $sliders = Slider::all();
        $homeContent = HomeContent::all()->first();

        $tags = Tag::all();
        $count = 0;
        foreach ($tags as $tag) {
            $count++;
            $tag['count'] = $count;

            $blogTags = BlogTag::where('tag_id', $tag->id)->get();

            if (count($blogTags)>0) {
                foreach ($blogTags as $blogTag) {
                    if ($blogTag['checked'] == 1) {
                        $blogImage = BlogImage::where('blog_id', $blogTag->blog_id)->first();
                        $tag['url'] = $blogImage->url;
                        
                        $blog = Blog::find($blogTag->blog_id);
                        $tag['blog_url'] = $blog->url;
                    }    
                }
                
            }
            
        }
        

        $blogs = Blog::all()->sortBy('created_at')->take(3);
        foreach ($blogs as $blog) {
            $blog['images'] = BlogImage::where('blog_id', $blog->id)->get();
            // $blog['description'] = Html2Text::convert($blog['description']);
        }
        
        $code = Code::all()->first();

        return view('index')->with([
            'sliders' => $sliders,
            'homeContent' => $homeContent,
            'tags' => $tags,
            'blogs' => $blogs,
            'code' => $code
        ]);
    }

    public function product() {
        $sliders = ProductSlider::all();
        $products = Product::all();

        $count = 0;
        foreach ($products as $product) {
            $count++;
            $product['count'] = $count;
            $product['images'] = ProductImage::where('product_id', $product->id)->get();
        }
        
        $code = Code::all()->first();

        return view('product')->with([
            'sliders' => $sliders,
            'products' => $products,
            'code' => $code
        ]);
    }

    public function contact() {
        $contact = Contact::all()->first();
        $code = Code::all()->first();

        return view('contact')->with([
            'contact' => $contact,
            'code' => $code
        ]); 
    }

    public function blog($name) {
        $blog = Blog::where('url','like','%'.$name.'%')->get()[0];
        $blog['images'] = BlogImage::where('blog_id', $blog->id)->get();
        $tag = BlogTag::where('blog_id', $blog->id)->get()[0]->tag_id;

        $blogIds = BlogTag::where('tag_id', $tag)->where('blog_id', '!=', $blog->id)->pluck('blog_id')->toArray();
        $blogs = Blog::whereIn('id', $blogIds)->take(3)->get();
        


        foreach ($blogs as $blog1) {
            $blog1['images'] = BlogImage::where('blog_id', $blog1->id)->get();
        }
        
        $code = Code::all()->first();

        return view('blog')->with([
            'blog' => $blog,
            'blogs' => $blogs,
            'code' => $code
        ]);
        // $tags = Blog
    }
}
