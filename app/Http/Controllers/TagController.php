<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use File;

use App\Product;
use App\Tag;
use App\BlogTag;

class TagController extends Controller
{
    //
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index() {
    	$tags = Tag::all();

    	$tag = Tag::find(1);

    	$products = Product::all();

    	return view('admin.tag.edit')->with([
          'url' => 'admin-tag-edit-1',

          'products' => $products,
          'tag' => $tag,

          'tags' => $tags
      	]);
    }

    public function editTag($id) {
    	$tags = Tag::all();

    	$tag = Tag::find($id);

    	$products = Product::all();

    	return view('admin.tag.edit')->with([
          'url' => 'admin-tag-edit-'.$id,

          'products' => $products,
          'tag' => $tag,

          'tags' => $tags
      	]);
    }

    public function updateTag(Request $request, $id) {
		$tag = Tag::find($id);

		$tag->value = $request->input('tag');
	        
	  $tag->save();


    // save product images
    $files = $request->file('images');
    if($request->hasFile('images')) {
        foreach ($files as $file) {
          $fileName = substr(md5($file->getClientOriginalName().date("Y-m-d h:i:sa")), 15).'.'.$file->getClientOriginalExtension();
          $file->move('uploads', $fileName);

          File::delete(public_path().$tag->url);
          $tag->url = '/uploads/'. $fileName;

          $tag->save();
          
        }
    }

    $files = $request->file('icons');
  	if($request->hasFile('icons')) {
  		foreach ($files as $file) {
		  	$fileName = substr(md5($file->getClientOriginalName().date("Y-m-d h:i:sa")), 15).'.'.$file->getClientOriginalExtension();
      		$file->move('uploads', $fileName);

      		File::delete(public_path().$tag->url_icon);
      		$tag->url_icon = '/uploads/'. $fileName;
      		$tag->save();
  		}
  	}

    return redirect('/admin-tag/edit/'.$id)->with('success_message', 'Cập nhật sản phẩm thành công');
	}  

  public function addFeatured($isChecked, $tagId, $blogId) {
  
    $blogTag = BlogTag::where(['blog_id' => $blogId, 'tag_id' => $tagId])->first();
    $blogTag->checked = (($isChecked == 'true') ? 1 : 0);
    $blogTag->save();

    return response()->json(['message'=>'success']);
  }
}
