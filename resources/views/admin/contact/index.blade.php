@extends('layouts.admin')

@section('header')
    <link href="{{ asset('frontend/css/plugins/summernote/summernote.css')}}" rel="stylesheet">
    <link href="{{asset('frontend/css/plugins/summernote/summernote-bs3.css')}}" rel="stylesheet">

@endsection

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Cập nhật contact</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/admin">Admin</a>
            </li>
            <li>
                <a href="/admin-contact">Quản lý contact</a>
            </li>
            <li class="active">
                <strong>Cập nhật contact</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Cập nhật contact</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form class="form-horizontal" method="POST" action="/admin-contact/update" enctype="multipart/form-data">
                        <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />

                        <div class="form-group"><label class="col-lg-2 control-label">Hình ảnh banner</label>
                            <div class="col-lg-10">
                                <input accept="image/*" type="file" class="blog-img-input" name="images[]" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-2"></div>
                            <div class="col-lg-10">
                                <img src="{{ ($contact) ? $contact->url : '' }}" height="100px">
                            </div>
                        </div>
                        
                        <div class="form-group"><label class="col-lg-2 control-label">Nội dung</label>
                            <div class="col-lg-10">
                                <textarea type="text" placeholder="content" name="content" style="height: 100px;" class="summernote">{{ ($contact) ? $contact->content : '' }}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-12">
                                <button type="submit" class="btn btn-primary pull-right">Cập nhật</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script src="{{ asset('frontend/js/jquery-3.1.1.min.js')}}"></script>
    <script src="{{ asset('frontend/js/bootstrap.min.js')}}"></script>
    <script src="{{ asset('frontend/js/plugins/metisMenu/jquery.metisMenu.js')}}"></script>
    <script src="{{ asset('frontend/js/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>
    

    <!-- SUMMERNOTE -->
    <script src="{{ asset('frontend/js/plugins/summernote/summernote.min.js')}}"></script>


    <script>
        $(document).ready(function() {
            
            $('.summernote').summernote({
                
                height:500,
                fontSizes: ['8', '9', '10', '11', '12', '14', '18','20','24','28','30','32','36','40'],
                toolbar: [
                    // [groupName, [list of button]]
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['font', ['strikethrough', 'superscript', 'subscript']],
                    ['fontsize', ['fontsize']],
                    ['fontname', ['fontname']],
                    ['style', ['style']],   
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['height', ['height']],
                    ['insert', ['link','image', 'doc', 'video', 'picture']], // image and doc are customized buttons
                  ],
                callbacks: {
                    onImageUpload: function(image) {
                        uploadImage(image[0]);
                    },
                    onMediaDelete : function ($target, $editable) {
                        // console.log();   // get image url 
                        deleteImage($target.attr('src'));

                    },
                    onPaste: function (e) {
                         var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
                         e.preventDefault();
                         setTimeout(function () {
                           document.execCommand('insertText', false, bufferText);
                         }, 10);
                       }
                }
            });

            function deleteImage(url) {
              var data = new FormData();
              data.append("url", url);
              var token = $('input[name=_token]').val();
              $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': token
                }
              });
              $.ajax({
                  url: '/admin-image/delete-img',
                  cache: false,
                  contentType: false,
                  processData: false,
                  data: data,
                  type: "post",

                  success: function(data) {
                    console.log(data);
                  },
                  error: function(data) {
                    console.log(data);
                  }
              });  
            }

            function uploadImage(image) {
              var data = new FormData();
              data.append("image", image);
              var token = $('input[name=_token]').val();
              $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': token
                }
              });
              $.ajax({
                  url: '/admin-image/create-img-url',
                  cache: false,
                  contentType: false,
                  processData: false,
                  data: data,
                  type: "post",
                  success: function(data) {
                      
                      var url = data['url'];
                      var image = $('<img>').attr({src: url, width: '100%'});
                      $('.summernote').summernote("insertNode", image[0]);
                  },
                  error: function(data) {
                      console.log(data);
                  }
              });
            }
            $('.dropdown-toggle').dropdown();
        });
    </script>
@endsection
