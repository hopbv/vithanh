<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use File;

use App\Product;
use App\ProductImage;

use App\Tag;

class ProductController extends Controller
{
    //
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index() {
     	$products = Product::all();

     	foreach ($products as $product) {
     		$product['images'] = ProductImage::where('product_id', $product->id)->get();
     		

     	}

      $product = Product::find(1);
      $productImage = ProductImage::where('product_id', 1)->get();

      $tags = Tag::all();

      return view('admin.product.edit')->with([
          'url' => 'admin-product-edit-1',

          'products' => $products,
          'product' => $product,
          'productImage' => $productImage,

          'tags' => $tags
      ]);
    }

    // public function addProduct(Request $request) {
    //     $product = new Product;
    //     $product->name = $request->input('name');
    //     $product->description = $request->input('description');
        
    //     $product->save();
    //     // save product images
    //     $files = $request->file('images');
    //     if($request->hasFile('images')) {
    //         foreach ($files as $file) {
    //           $fileName = substr(md5($file->getClientOriginalName().date("Y-m-d h:i:sa")), 15).'.'.$file->getClientOriginalExtension();
    //           $file->move('uploads', $fileName);
    //           $productImage = new ProductImage;
    //           $productImage->url = '/uploads/'. $fileName;
    //           $productImage->product_id = $product->id;


    //           // save product icons
		  //     $files = $request->file('icons');
		  //     if($request->hasFile('icons')) {
	   //          foreach ($files as $file) {
	   //            $fileName = substr(md5($file->getClientOriginalName().date("Y-m-d h:i:sa")), 15).'.'.$file->getClientOriginalExtension();
	   //            $file->move('uploads', $fileName);
	   //            $productImage->url_icon = '/uploads/'. $fileName;
	   //          }
	   //         }
    //           $productImage->save();
    //         }
    //     }

        

    //     $tag = new ProductTag;
    //     $tag->product_id = $product->id;
    //     $tag->ptag_id = $request->input('ptag');
    //     $tag->save();

    //     return redirect('/admin-product')->with('success_message', 'Đăng sản phẩm thành công');
    // }

	public function editProduct($id) {
    $products = Product::all();

		$product = Product::find($id);
		

		$productImage = ProductImage::where('product_id', $id)->get();

    $tags = Tag::all();


		return view('admin.product.edit')->with([
			'url' => 'admin-product-edit-'.$id,
      'products' => $products,

			'product' => $product,
			'productImage' => $productImage,

      'tags' => $tags
		]);
	}  


	public function updateProduct(Request $request, $id) {
		$product = Product::find($id);

		$product->name = $request->input('name');
    $product->description = $request->input('description');
    $product->tag = $request->input('tag');
        
    $product->save();


    // save product images
    $files = $request->file('images');
    if($request->hasFile('images')) {
        foreach ($files as $file) {
          $fileName = substr(md5($file->getClientOriginalName().date("Y-m-d h:i:sa")), 15).'.'.$file->getClientOriginalExtension();
          $file->move('uploads', $fileName);
          $productImage = ProductImage::where('product_id', $product->id)->first();

          if ($productImage) {
          	File::delete(public_path().$productImage->url);
            $productImage->url = '/uploads/'. $fileName;
            $productImage->product_id = $product->id;
            $productImage->save();
          } else {
            $productImage = new ProductImage;
            $productImage->url = '/uploads/'. $fileName;
            $productImage->product_id = $product->id;

            $productImage->save();
          }
        }
    }

    return redirect('/admin-product/edit/'.$id)->with('success_message', 'Cập nhật sản phẩm thành công');
	}  

}
