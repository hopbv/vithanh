@extends('layouts.admin')

@section('header')
    <link href="{{ asset('frontend/css/plugins/chosen/bootstrap-chosen.css') }}" rel="stylesheet">

@endsection

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Cập nhật category</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/admin">Admin</a>
            </li>
            <li>
                <a href="/admin-tag">Quản lý category</a>
            </li>
            <li class="active">
                <strong>Cập nhật category</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Sửa category</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form class="form-horizontal" method="POST" action="/admin-tag/update/{{$tag->id}}" enctype="multipart/form-data">
                        <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                        <div class="form-group"><label class="col-lg-2 control-label">Tên category</label>
                            <div class="col-lg-10"><input type="text" placeholder="tag" name="tag" class="form-control" value="{{ $tag->value }}">
                            </div>
                        </div>
                        <div class="form-group"><label class="col-lg-2 control-label">Hình ảnh</label>
                            <div class="col-lg-10">
                                <input accept="image/*" type="file" class="tag-img-input" name="images[]">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-2"></div>
                            <div class="col-lg-10">
                                <img src="{{$tag->url}}" width="100">
                            </div>
                        </div>

                        <div class="form-group"><label class="col-lg-2 control-label">Hình ảnh icon</label>
                            <div class="col-lg-10">
                                <input accept="image/*" type="file" class="tag-img-input" name="icons[]">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-2"></div>
                            <div class="col-lg-10">
                                <img style="background-color: #989595" src="{{$tag->url_icon}}" width="100">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-12">
                                <button type="submit" class="btn btn-primary pull-right">Cập nhật</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script src="{{ asset('frontend/js/jquery-3.1.1.min.js')}}"></script>
    <script src="{{ asset('frontend/js/bootstrap.min.js')}}"></script>
    <script src="{{ asset('frontend/js/plugins/metisMenu/jquery.metisMenu.js')}}"></script>
    <script src="{{ asset('frontend/js/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>
    
    <!-- Chosen -->
    <script src="{{ asset('frontend/js/plugins/chosen/chosen.jquery.js') }}"></script>

    <script>
        $(document).ready(function() {
            
            $('.chosen-select').chosen({width: "100%"});
            
        });
    </script>
@endsection
