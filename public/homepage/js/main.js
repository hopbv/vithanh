$(document).ready(function () {
    var link = window.location.href;
    link = link.split('/');
    link = link[0] + '//' + link[2] + '/';
    $('#brand-carousel').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        arrows: true,
        autoplay: true,
        autoplaySpeed: 5000,
        infinite: true,
        speed: 600,
        prevArrow: $('.left-arrow'),
        nextArrow: $('.right-arrow')
    });
    $('#brand-carousel-product').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false,
        arrows: true,
        autoplay: true,
        autoplaySpeed: 5000,
        infinite: true,
        speed: 600,
        prevArrow: $('.left-arrow'),
        nextArrow: $('.right-arrow')
    });
    var mq = window.matchMedia("(max-width: 575px)");
    if (mq.matches) {
        $('#partner-carousel').slick({
            slidesToShow: 5,
            slidesToScroll: 1,
            dots: false,
            arrows: false,
            infinite: true,
            speed: 300,
            prevArrow: $('.left-arrow'),
            nextArrow: $('.right-arrow')
        });
    }
    else {
        $('#partner-carousel').slick({
            slidesToShow: 5,
            slidesToScroll: 1,
            dots: false,
            arrows: false,
            infinite: true,
            speed: 300,
            prevArrow: $('.left-arrow'),
            nextArrow: $('.right-arrow')
        });
    }
    // $(".drop-info-1").css("opacity", "0");
    // $(".drop-info-2").css("opacity", "0");
    // $(".drop-info-3").css("opacity", "0");
    // $(".drop-info-4").css("opacity", "0");

    $(".drop-info-1").hide();
    $(".drop-info-2").hide();
    $(".drop-info-3").hide();
    $(".drop-info-4").hide();

    $(".drop-mobile-1").hide();
    $(".drop-mobile-2").hide();
    $(".drop-mobile-3").hide();
    $(".drop-mobile-4").hide();

    $(".row12").hide();
    $(".row34").hide();
    $(".drop1").click(function () {
        if ($(".descript-container .arrow1").attr("src") == link + "homepage/img/dropdown-arrow.png")
            $(".descript-container .arrow1").attr("src", link + "homepage/img/dropdown-arrow-down.png")
        else
            if ($(".descript-container .arrow1").attr("src") == link + "homepage/img/dropdown-arrow-down.png")
                $(".descript-container .arrow1").attr("src", link + "homepage/img/dropdown-arrow.png");
        $(".drop-mobile-1").slideToggle();
        $(".drop-info-1").slideToggle();
    });
    $(".drop2").click(function () {
        if ($(".descript-container .arrow2").attr("src") == link + "homepage/img/dropdown-arrow.png")
            $(".descript-container .arrow2").attr("src", link + "homepage/img/dropdown-arrow-down.png")
        else
            if ($(".descript-container .arrow2").attr("src") == link + "homepage/img/dropdown-arrow-down.png")
                $(".descript-container .arrow2").attr("src", link + "homepage/img/dropdown-arrow.png");
        $(".drop-mobile-2").slideToggle();
        $(".drop-info-2").slideToggle();
    });
    $(".drop3").click(function () {
        if ($(".descript-container .arrow3").attr("src") == link + "homepage/img/dropdown-arrow.png")
            $(".descript-container .arrow3").attr("src", link + "homepage/img/dropdown-arrow-down.png")
        else
            if ($(".descript-container .arrow3").attr("src") == link + "homepage/img/dropdown-arrow-down.png")
                $(".descript-container .arrow3").attr("src", link + "homepage/img/dropdown-arrow.png");
        $(".drop-mobile-3").slideToggle();
        $(".drop-info-3").slideToggle();
    });
    $(".drop4").click(function () {
        if ($(".descript-container .arrow4").attr("src") == link + "homepage/img/dropdown-arrow.png")
            $(".descript-container .arrow4").attr("src", link + "homepage/img/dropdown-arrow-down.png")
        else
            if ($(".descript-container .arrow4").attr("src") == link + "homepage/img/dropdown-arrow-down.png")
                $(".descript-container .arrow4").attr("src", link + "homepage/img/dropdown-arrow.png");
        $(".drop-mobile-4").slideToggle();
        $(".drop-info-4").slideToggle();
    });

    // $(function() {
    //     var x = document.getElementsByClassName("img-solution");
    //     for (let i = 0; i < x.length; i++) {
    //         x[i].style.height = Math.floor(x[i].offsetWidth * 1.0) + 'px';
    //     }
    // });

    $(function() {
        var x = document.getElementsByClassName("solution-item");
        for (let i = 0; i < x.length; i++) {
            x[i].style.height = Math.floor(x[i].offsetWidth * 1.0000) + 'px';
            console.log(x[i].offsetWidth);
            console.log(x[i].style.height);
        }
    });

    $(function() {
        var x = document.querySelectorAll("#solution-section .img-container");
        for (let i = 0; i < x.length; i++) {
            x[i].style.height = Math.floor(x[i].offsetWidth * 9 / 16) + 'px';
            console.log(x[i].offsetWidth);
            console.log(x[i].style.height);
        }

        x = document.querySelectorAll("#body-section-contact .img-container");
        for (let i = 0; i < x.length; i++) {
            x[i].style.height = Math.floor(x[i].offsetWidth * 9 / 16) + 'px';
            console.log(x[i].offsetWidth);
            console.log(x[i].style.height);
        }

        var height = $('#brand-carousel-product .img-container').css('height');
        $('#brand-carousel-product .fullscreen-bg-video').css('height', height);
    });

    $('.fullscreen-bg-video').on('play', function() {
        $('.left-arrow').hide();
        $('.right-arrow').hide();

        $('.slick-dots').hide();
    })

    $('.fullscreen-bg-video').on('pause', function() {
        $('.left-arrow').show();
        $('.right-arrow').show();

        $('.slick-dots').show();
    }) 

    $('.fullscreen-bg-video').on('ended', function() {
        $('.left-arrow').show();
        $('.right-arrow').show();

        $('.slick-dots').show();
    });
    $('#brand-carousel-product .fullscreen-bg-video')[0].play(); 
    $('#brand-carousel-product').on('afterChange', function(event, slick, currentSlide, nextSlide){
        $('#brand-carousel-product .fullscreen-bg-video')[0].play(); 
    });
})