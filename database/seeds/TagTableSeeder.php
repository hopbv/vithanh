<?php

use Illuminate\Database\Seeder;

class TagTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tags')->insert([
            'value' => 'Thấm hút',
        ]);

        DB::table('tags')->insert([
            'value' => 'Khử mùi',
        ]);

        DB::table('tags')->insert([
            'value' => 'Bảo quản',
        ]);

        DB::table('tags')->insert([
            'value' => 'Chuyên biệt',
        ]);
    }
}
