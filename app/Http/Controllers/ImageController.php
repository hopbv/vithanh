<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use File;



class ImageController extends Controller
{
    //
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function createImgUrl(Request $request) {
    	$image = $_FILES['image']; 

    	$ext = explode('.', $image['name'])[1];
    	$name = explode('.', $image['name'])[0];
    	$fileName = substr(md5($name.date("Y-m-d h:i:sa")), 15).'.'.$ext;

    	$destination  = public_path().'/uploads/'.$fileName;
    	move_uploaded_file($image["tmp_name"], $destination);

    	$url = '/uploads/'.$fileName;

    	return response()->json([
    		'url' => $url
    	]);
    }

    public function deleteImg(Request $request) {
    	$destination = $_POST['url'];
    	print_r(unlink(public_path().$destination));

    	return response()->json([
    		'message' => 'success'
    	]);
    }
}
