@extends('layouts.app')

@section('title')
<title>Miếng Thấm Hút Thực Phẩm Cao Cấp - Bài viết</title>
@endsection

@section('content')
<section id="body-section">
    <section id="solution-section">
        <div class="row">
            
            <div class="img-container col-sm-12" style="background-image: url({{($blog->images) ? $blog->images[0]->url : ''}}); ">
                <!-- <img src="img/slide-4.png" alt=""> -->
            </div>
            <div class="col-sm-1"></div>
            <div class="content col-sm-10">
                <div class="header">
                    <div class="description">
                        <br>
                        <p class="title2">{{$blog->name}}</p>
                        <br>
                    </div>
                </div>
            </div>
            <div class="col-sm-12"></div>
            <div class="col-sm-1"></div>
            <div class="content col-sm-10">
                <div class="intro">{!!$blog->description!!}
                </div>
            </div>
        </div>
    </section>
    <section id="article-section-news">
        <div class="header">
            <div class="row">
                <div class="col-sm-1"></div>
                <div class="content-container title col-sm-10">
                    <div class="start-line">
                        <div class="line-top"></div>
                        <a href="{{$blog->url}}">
                            <div class="content">BÀI VIẾT CHỌN LỌC</div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="article-list">
            @if (count($blogs) > 0)
                @foreach($blogs as $blog)
                    <div class="row">
                        <div class="col-sm-1"></div>
                        <a href="#">
                            <div class="header col-sm-10">
                                <div class="row">
                                    <div class="img-container col-sm-6">
                                        <div class="end-line">
                                            <img src="{{($blog->images) ? $blog->images[0]->url : ''}}">
                                            <div class="line-bottom"></div>
                                        </div>
                                    </div>
                                    <div class="content-container col-sm-6">
                                        <div class="paragraph">{{$blog->name}}</div>
                                        <div class="description-article">{!!mb_substr(strip_tags($blog->description), 0, 200)!!}...</div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                @endforeach
            @endif
        </div>
    </section>
</section>
@endsection