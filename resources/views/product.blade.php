@extends('layouts.app')

@section('title')
<title>Miếng Thấm Hút Thực Phẩm Cao Cấp - Sản phẩm</title>
@endsection

@section('content')
<section id="body-section-product">
    <section id="slide-container-product">
        <div class="left-arrow">
            <div class="arrow-container">
                <img src="{{ asset('homepage/img/2.png') }}">
            </div>
        </div>
        <div id="brand-carousel-product">
            @if (count($sliders) > 0)
                @foreach($sliders as $slider)
                    <div class="brand-item-carousel">
                        @php
                            $extension = explode('.',$slider->url)[1];
                        @endphp
                        @if ($extension == 'png' || $extension == 'jpg' || $extension == 'jpeg')
                            <div class="img-container" style="background-image: url('{{$slider->url}}')">
                                <!-- <img src="img/slide-1.png"> -->
                            </div>
                        @endif
                        @if ($extension == 'mp4')
                            <div class="fullscreen-bg">
                                <video class="fullscreen-bg-video" muted autoplay>
                                    <source src="{{$slider->url}}" type="video/mp4">
                                </video>    
                            </div>
                        @endif
                    </div>
                @endforeach
            @endif
        </div>
    </section>
    <section id="introduction">
        <div class="row">
            <div class="col-sm-1"></div>
            <div class="content col-sm-10">
                <div class="header">
                    <div class="description">
                        <p class="title1">SẢN PHẨM ĐA DẠNG</p>
                        <p class="title2">ĐÁP ỨNG AN TOÀN THỰC PHẨM</p>
                        <div class="end-line">
                            <p class="title3">
                                TIÊU CHUẨN CHÂU ÂU
                            </p>
                            <div class="line-bottom"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="product">
        <div class="row">
            <div class="row col-sm-12">
                <div class="col-sm-1"></div>
                <div class="product-item product1 col-sm-10 row">
                    <img class="drop1 img-solution col-sm-6" src="{{($products[0]->images) ? $products[0]->images[0]->url : ''}}" alt="">
                    <div class="header col-sm-6">
                        <div class="descript-container drop1">
                            <div class="tab-container">
                                <p class="descript">{{$products[0]->name}}</p>
                                <span>
                                    <img class=" dropdown-arrow arrow1" src="{{asset('homepage/img/dropdown-arrow.png')}}" alt="">
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="drop-info-1 drop col-sm-12 row">
                    <div class="col-sm-1"></div>
                    <div class="col-sm-10">
                        {!! $products[0]->description !!}
                    </div>
                </div>
            </div>
            <div class="row col-sm-12">
                <div class="col-sm-1"></div>
                <div class="product-item product2 col-sm-10 row">
                <img class="drop2 img-solution col-sm-6" src="{{($products[1]->images) ? $products[1]->images[0]->url : ''}}" alt="">
                    <div class="header col-sm-6">
                        <div class="descript-container drop2">
                            <div class="tab-container">
                                <p class="descript">{{$products[1]->name}}</p>
                                <img class="dropdown-arrow arrow2" src="{{asset('homepage/img/dropdown-arrow.png')}}" alt="">
                            </div>
                        </div>
                    </div>
                    
                </div>
                <div class="drop-info-2 drop col-sm-12 row">
                    <div class="col-sm-1"></div>
                    <div class="col-sm-10">
                        {!! $products[1]->description !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="row col-sm-12">
                <div class="col-sm-1"></div>
                <div class="product-item product3 col-sm-10 row">
                        <img class="drop3 img-solution col-sm-6" src="{{($products[2]->images) ? $products[2]->images[0]->url : ''}}" alt="">
                    <div class="header col-sm-6">
                        <div class="descript-container drop3">
                            <div class="tab-container">
                                <p class="descript">{{$products[2]->name}}</p>
                                <img class="dropdown-arrow arrow3" src="{{asset('homepage/img/dropdown-arrow.png')}}" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="drop-info-3 drop col-sm-12 row">
                    <div class="col-sm-1"></div>
                    <div class="col-sm-10">
                        {!! $products[2]->description !!}
                    </div>
                </div>
            </div>
            <div class="row col-sm-12">
                <div class="col-sm-1"></div>
                <div class="product-item product4 col-sm-10 row">
                    <img class="drop4 img-solution col-sm-6" src="{{($products[3]->images) ? $products[3]->images[0]->url : ''}}" alt="">
                    <div class="header col-sm-6">
                        <div class="descript-container drop4">
                            <div class="tab-container">
                                <p class="descript">{{$products[3]->name}}</p>
                                <img class="dropdown-arrow arrow4" src="{{asset('homepage/img/dropdown-arrow.png')}}" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="drop-info-4 drop col-sm-12 row">
                    <div class="col-sm-1"></div>
                    <div class="col-sm-10">
                        {!! $products[3]->description !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>
@endsection