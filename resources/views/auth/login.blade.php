
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Vi Thanh | Login</title>

    <link href="{{ asset('frontend/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/font-awesome/css/font-awesome.css') }}" rel="stylesheet">

    <link href="{{ asset('frontend/css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/style.css') }}" rel="stylesheet">
    <link rel="shortcut icon" href="http://vithanh.pixiostudio.com/homepage/img/logo.png" type="image/x-icon"/>
    <meta property="og:image" content="https://cdn0.iconfinder.com/data/icons/social-media-2092/100/social-35-512.png"/>
	<meta property="og:image:secure_url" content="https://cdn0.iconfinder.com/data/icons/social-media-2092/100/social-35-512.png" />

</head>

<body class="gray-bg">

    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <div>

                <h1 class="logo-name" style="margin-left: -30px;">Pixio</h1>

            </div>
            <h3>Welcome to Vi Thanh</h3>
            <p>Login</p>
            @if(session('error'))
                <p style="color: #ed5565;">
                    {{session('error')}}
              </p>
            @endif
            <form method="POST" class="m-t" role="form" action="/user-login">
                <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                <div class="form-group">
                    <input type="email" name="email" class="form-control" placeholder="Username" required="">
                </div>
                <div class="form-group">
                    <input type="password" name="password" class="form-control" placeholder="Password" required="">
                </div>
                <button type="submit" class="btn btn-primary block full-width m-b">Login</button>
            </form>
            <p class="m-t"> <small>ViThanh &copy; <a href="https://pixiostudio.com/">Pixio Studio</a> 2018</small> </p>
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="{{ asset('frontend/js/jquery-3.1.1.min.js') }}"></script>
    <script src="{{ asset('frontend/js/bootstrap.min.js') }}"></script>

</body>

</html>
