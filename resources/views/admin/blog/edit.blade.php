@extends('layouts.admin')

@section('header')
    <link href="{{ asset('frontend/css/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/plugins/chosen/bootstrap-chosen.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/plugins/summernote/summernote.css')}}" rel="stylesheet">
    <!-- <link href="{{asset('css/plugins/summernote/summernote-bs3.css')}}" rel="stylesheet"> -->

    <!-- Sweet Alert -->
    <link href="{{ asset('frontend/css/plugins/sweetalert/sweetalert.css') }}" rel="stylesheet">


@endsection

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Cập nhật bài viết</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/admin">Admin</a>
            </li>
            <li>
                <a href="/admin-blog">Quản lý bài viết</a>
            </li>
            <li class="active">
                <strong>Cập nhật bài viết</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Sửa bài viết</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form class="form-horizontal" method="POST" action="/admin-blog/update/{{$blog->id}}" enctype="multipart/form-data">
                        <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                        <div class="form-group"><label class="col-lg-2 control-label">Link</label>
                            <div class="col-lg-10"><input type="text" placeholder="" name="url" class="form-control" value="{{ $blog->url}}">
                            </div>
                        </div>
                        <div class="form-group"><label class="col-lg-2 control-label">Tên bài viết</label>
                            <div class="col-lg-10"><input type="text" placeholder="blog" name="name" class="form-control" value="{{ $blog->name }}" required="">
                            </div>
                        </div>
                        <div class="form-group"><label class="col-lg-2 control-label">Category</label>
                            <div class="col-lg-10">
                                <select data-placeholder="Choose a tag..." class="chosen-select" tabindex="4"; name="tag">
                                    <option value="">Select</option>
                                    <option value="{{ $blog->tag->id }}" selected="">{{ $blog->tag->value }}</option>
                                    @if(count($arrTags) > 0)
                                        @foreach ($arrTags as $tag)
                                            <option value="{{ $tag->id }}">{{ $tag->value }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="form-group"><label class="col-lg-2 control-label">Nội dung</label>
                            <div class="col-lg-10">
                                <textarea type="text" placeholder="description" name="description" style="height: 100px;" class="form-control summernote">{{ $blog->description }}</textarea>
                            </div>
                        </div>
                        <div class="form-group"><label class="col-lg-2 control-label">Hình ảnh</label>
                            <div class="col-lg-10">
                                <input accept="image/*" type="file" class="blog-img-input" name="images[]">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-2"></div>
                            <div class="col-lg-10">
                                <img src="{{$blogImage[0]->url}}" width="100">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-12">
                                <button type="submit" class="btn btn-primary pull-right">Cập nhật</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Danh sách bài viết</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">

                    <table class="table dataTables">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th width="50%">Tiêu đề</th>
                            <th>Hình ảnh</th>
                            <th>Category</th>
                            <th></th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                            @if(count($blogs) > 0)
                                @foreach ($blogs as $blog)
                                <tr>
                                    <td>{{$blog->id}}</td>
                                    <td>{{$blog->name}}</td>
                                    <td>
                                        <img src="{{$blog->images[0]->url}}" width="100">
                                    </td>
                                    <td>{{$blog->tag->value}}</td>
                                    <td>
                                        <input type="checkbox" name="featured{{$blog->tag->id}}" data-tag-id="{{$blog->tag->id}}" data-blog-id="{{$blog->id}}" {{($blog->checked) ? 'checked' : ''}}>
                                    </td>
                                    <td>
                                        <a href="/admin-blog/edit/{{$blog->id}}" class="btn btn-warning btn-custom">Edit</a>
                                        <button class="btn btn-danger btn-custom btn-delete" data-id="{{$blog->id}}">Delete</button>
                                    </td>
                                </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script src="{{ asset('frontend/js/jquery-3.1.1.min.js')}}"></script>
    <script src="{{ asset('frontend/js/bootstrap.min.js')}}"></script>
    <script src="{{ asset('frontend/js/plugins/metisMenu/jquery.metisMenu.js')}}"></script>
    <script src="{{ asset('frontend/js/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{ asset('frontend/js/plugins/dataTables/datatables.min.js') }}"></script>
    <!-- Tags Input -->
    <script src="{{ asset('frontend/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js') }}"></script>
    <!-- Chosen -->
    <script src="{{ asset('frontend/js/plugins/chosen/chosen.jquery.js') }}"></script>

    <!-- SUMMERNOTE -->
    <script src="{{ asset('frontend/js/plugins/summernote/summernote.min.js')}}"></script>

    <!-- Sweet alert -->
    <script src="{{ asset('frontend/js/plugins/sweetalert/sweetalert.min.js') }}"></script>


    <script>
        $(document).ready(function() {
            
            $('.tagsinput').tagsinput({
                tagClass: 'label label-primary'
            });
            $('.chosen-select').chosen({width: "100%"});
            $('.summernote').summernote({
                
                height:500,
                fontSizes: ['8', '9', '10', '11', '12', '14', '18','20','24','28','30','32','36','40'],
                toolbar: [
                    // [groupName, [list of button]]
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['font', ['strikethrough', 'superscript', 'subscript']],
                    ['fontsize', ['fontsize']],
                    ['fontname', ['fontname']],
                    ['style', ['style']],   
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['height', ['height']],
                    ['insert', ['link','image', 'doc', 'video', 'picture']], // image and doc are customized buttons
                  ],
                callbacks: {
                    onImageUpload: function(image) {
                        uploadImage(image[0]);
                    },
                    onMediaDelete : function ($target, $editable) {
                        // console.log();   // get image url 
                        deleteImage($target.attr('src'));

                    },
                    onPaste: function (e) {
                         var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
                         e.preventDefault();
                         setTimeout(function () {
                           document.execCommand('insertText', false, bufferText);
                         }, 10);
                       }
                }
            });

            function deleteImage(url) {
              var data = new FormData();
              data.append("url", url);
              var token = $('input[name=_token]').val();
              $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': token
                }
              });
              $.ajax({
                  url: '/admin-image/delete-img',
                  cache: false,
                  contentType: false,
                  processData: false,
                  data: data,
                  type: "post",

                  success: function(data) {
                    console.log(data);
                  },
                  error: function(data) {
                    console.log(data);
                  }
              });  
            }

            function uploadImage(image) {
              var data = new FormData();
              data.append("image", image);
              var token = $('input[name=_token]').val();
              $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': token
                }
              });
              $.ajax({
                  url: '/admin-image/create-img-url',
                  cache: false,
                  contentType: false,
                  processData: false,
                  data: data,
                  type: "post",
                  success: function(data) {
                      
                      var url = data['url'];
                      var image = $('<img>').attr({src: url, width: '100%'});
                      $('.summernote').summernote("insertNode", image[0]);
                  },
                  error: function(data) {
                      console.log(data);
                  }
              });
            }
            $('.dropdown-toggle').dropdown();
        });
    </script>

    <script>
        function xoa_dau(str) {
            str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
            str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
            str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
            str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
            str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
            str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
            str = str.replace(/đ/g, "d");
            str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, "A");
            str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, "E");
            str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, "I");
            str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, "O");
            str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, "U");
            str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, "Y");
            str = str.replace(/Đ/g, "D");
            return str;
        }

        function xoa_space(str) {
            // Gộp nhiều dấu space thành 1 space
            str = str.replace(/\s+/g, ' ');
            // loại bỏ toàn bộ dấu space (nếu có) ở 2 đầu của xâu
            str.trim();
            return str;
        }

        function converStrtToLink(str) {
            str = xoa_dau(str);
            str = str.replace(/[^0-9a-zàáạảãâầấậẩẫăằắặẳẵèéẹẻẽêềếệểễìíịỉĩòóọỏõôồốộổỗơờớợởỡùúụủũưừứựửữỳýỵỷỹđ\s]/gi, '');
            str = xoa_space(str);
            str = str.toLowerCase(str);

            str = str.replace(/\s/g,'-');
            return str;
        }

        $('input[name=name]').keyup(function(e) {
            var name = $(this).val();

            name = converStrtToLink(name);

            var link = window.location.href;
            
            link = '/blog/'+name;
            
            if (name != '')
                $('input[name=url]').val(link);
            else
                $('input[name=url]').val('');
        })
    </script>

    <script>
        $(document).ready(function() {
            $('input[type=checkbox]').on('change', function(e) {
                e.preventDefault();

                var isChecked = $(this).prop('checked');

                var tagId = $(this).data('tag-id');
                var blogId = $(this).data('blog-id');
                
                $.ajax({
                    url             :           '/admin-blog-tag/addFeatured/'+isChecked+'/'+tagId+'/'+blogId,
                    type            :           'get',

                    error           :           function(err) {
                        console.log(err);
                        setTimeout(function() {
                            toastr.options = {
                                closeButton: true,
                                progressBar: true,
                                showMethod: 'slideDown',
                                timeOut: 1200
                            };
                            toastr.danger('Lỗi cập nhật bài viết lên top!', 'Vi Thanh Admin');
                        }, 300);
                    },

                    success         :           function(data) {
                        console.log(data);
                        setTimeout(function() {
                            toastr.options = {
                                closeButton: true,
                                progressBar: true,
                                showMethod: 'slideDown',
                                timeOut: 1200
                            };
                            toastr.success('Đã cập nhật bài viết lên top thành công', 'Vi Thanh Admin');
                        }, 300);
                    }
                })
            })
        })
    </script>

    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function(){
            $('.dataTables').DataTable({
                pageLength: 10,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons:[],
                aoColumnDefs : [ {
                    //We want to hide sorting arrows for Position colum:
                   orderable : false, aTargets : [2,4,5]        
                }],

            });

        });

    </script>

    <script>
        $('.btn-delete').click(function () {
            var id = $(this).data('id');
            swal({
                        title: "Bạn có chắc chắn xóa không?",
                        text: "Bài viết sau khi xóa sẽ không hồi phục lại được.",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Có",
                        cancelButtonText: "Không",
                        closeOnConfirm: false,
                        closeOnCancel: false },
                    function (isConfirm) {
                        if (isConfirm) {
                            swal("Đã xóa!", "Bài viết đã được xóa.", "success");
                            window.location.href="/admin-blog/delete/"+id;
                        } else {
                            swal("Đã hủy", "Bài viết đã được giữ lại", "error");
                        }
                    });
        });
    </script>
@endsection
